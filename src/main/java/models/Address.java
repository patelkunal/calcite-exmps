package models;

/**
 * Created by kunal_patel on 25/11/15.
 */
public class Address {

	public long personId;
	public String city;

	public Address(long personId, String city) {
		this.personId = personId;
		this.city = city;
	}
}
