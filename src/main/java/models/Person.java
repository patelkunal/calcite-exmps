package models;

/**
 * Created by kunal_patel on 25/11/15.
 */
public class Person {

	public long id;
	public String firstName;
	public String lastName;

	public Person(String firstName, long id, String lastName) {
		this.firstName = firstName;
		this.id = id;
		this.lastName = lastName;
	}
}
