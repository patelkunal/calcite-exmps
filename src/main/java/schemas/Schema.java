package schemas;

import models.Address;
import models.Person;

/**
 * Created by kunal_patel on 25/11/15.
 */
public class Schema {

	public Person[] persons;

	public Address[] addresses;

	public static Schema getInstance() {
		Schema schema = new Schema();
		schema.persons = new Person[1];
		schema.addresses = new Address[2];

		schema.persons[0] = new Person("kppatel", 1, "patel");
		schema.addresses[0] = new Address(1, "BLR");
		schema.addresses[1] = new Address(1, "AMD");

		return schema;
	}

}
