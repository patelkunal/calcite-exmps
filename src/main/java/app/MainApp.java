package app;

import utilities.CalciteUtils;

import java.sql.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by kunal_patel on 25/11/15.
 */
public class MainApp {

	public static void main(String[] args) {
		try {
			Connection conn = CalciteUtils.getConnection();
			String query = "select * from persons p";

			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery(query);
			ResultSetMetaData metaData = resultSet.getMetaData();
			System.out.println(metaData);
			System.out.println(metaData.getColumnCount());
			while (resultSet.next()) {
				IntStream.rangeClosed(1, metaData.getColumnCount()).boxed().forEach(index -> {
					try {
						System.out.println(resultSet.getMetaData().getColumnName(index) + " => " + resultSet.getString(index));
					} catch (SQLException e) {
						e.printStackTrace();
					}
				});
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
