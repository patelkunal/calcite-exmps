package utilities;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by kunal_patel on 25/11/15.
 */
public class CalciteUtils {

	public static Connection getConnection() throws Exception {
		Class.forName("org.apache.calcite.jdbc.Driver");
		Properties info = new Properties();
		info.setProperty("lex", "JAVA");
		info.put("model", CalciteUtils.getPath("model.json"));
		Connection connection = DriverManager.getConnection("jdbc:calcite:", info);
		return connection;
	}

	public static String getPath(String filePath) {
		URL url = CalciteUtils.class.getClass().getResource(filePath);
		if (url == null)
			url = CalciteUtils.class.getClassLoader().getResource(filePath);
		String s = url.toString();
		return s.startsWith("file:") ? s.substring("file:".length()) : s;
	}


}
