package commons;

import org.junit.Test;
import utilities.CalciteUtils;

import java.net.URL;
import java.sql.*;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertNotNull;

/**
 * Created by kunal_patel on 25/11/15.
 */
public class CalciteMainTest {

	@Test
	public void testConnection() throws SQLException {
		Properties props = new Properties();
		props.put("model", CalciteUtils.getPath("model.json"));
		System.out.println("model = " + props.get("model"));
		Connection conn = DriverManager.getConnection("jdbc:calcite:", props);
		assertNotNull(conn);
	}

	@Test
	public void testTableMetadata() throws SQLException {
		Properties props = new Properties();
		props.put("model", CalciteUtils.getPath("model.json"));
		Connection conn = DriverManager.getConnection("jdbc:calcite:", props);
		assertNotNull(conn);
		DatabaseMetaData metaData = conn.getMetaData();
		ResultSet resultSet = metaData.getTables(null, "Persons", "%", null);
		if (resultSet.next()) {
			System.out.println(resultSet);
			System.out.println(resultSet.getString(1));
			System.out.println(resultSet.getString(2));
			System.out.println(resultSet.getString(3));
		} else
			System.out.println("No data in result set !!");

	}

	@Test
	public void findModel() {
		URL url = this.getClass().getResource("/model.json");
		System.out.println(CalciteUtils.getPath("/model.json"));
		System.out.println(CalciteUtils.getPath("model.json"));
		System.out.println("url = " + url.toString());
	}

	@Test
	public void testJava8() {
		System.out.println(IntStream.rangeClosed(1, 5).boxed().collect(Collectors.toList()));
	}
}
